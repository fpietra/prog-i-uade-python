def crearMatriz(dim):
    """Crea una matriz de NxN"""
    matriz=[]
    for f in range(dim):
        matriz.append([0]*dim)
    return matriz

def cargarMatriz(dim, matriz):
    """Carga números enteros en una matriz de N x N,
    ingresando los datos desde teclado."""
    for f in range(dim):
        for c in range(dim):
            num=int(input("Ingrese un numero entero: "))
            matriz[f][c]=num
            print(f,c)
    return matriz

def ordenarMatriz(dim, matriz):
    """Ordena en forma ascendente cada una de las filas de la matriz."""
    f=0
    while f < dim:
        for lim in range (dim-1,0,-1):
            for col in range(lim):
                if matriz[f][col]>matriz[f][col+1]:
                    num=matriz[f][col]
                    matriz[f][col]=matriz[f][col+1]
                    matriz[f][col+1]=num
        f+=1
    return matriz

def intercambiarFilas (matriz,dim,f1,f2):
    """Intercambia dos filas, cuyos números se reciben como parámetro"""
    f1-=1
    f2-=1
    filaRes=[]
    for c in range(dim):
        filaRes.append(matriz[f1][c])
    for c in range(dim):
        matriz[f1][c]=matriz[f2][c]
    for c in range(dim):
        matriz[f2][c]=filaRes[c]
    return matriz

def intercambiarColumnas (matriz,dim,c1,c2):
    """Intercambia dos columnas dadas, cuyos números se reciben como parámetro"""
    c1-=1
    c2-=1
    vector=[]
    for f in range(dim):
        vector.append(matriz[f][c1])
    for f in range(dim):
        matriz[f][c1]=matriz[f][c2]
    for f in range(dim):
        matriz[f][c2]=vector[f] 
    return matriz

def intercambiarFilaCol(matriz,dim,f,c):
    """Intercambiar una fila por una columna,
    cuyos números se reciben como parámetro"""
    f-=1
    c-=1
    listaCol=[]
    for columna in range(dim):
        listaCol.append(matriz[f][columna]) #La columna esta en la lista
    for columna in range(dim):
        matriz[f][columna]=matriz[columna][c]
    for fila in range(dim):
        matriz[fila][c]=listaCol[fila]
    return matriz

def trasponerMatriz(matriz,dim):
    """Transpone la matriz sobre si misma. """
    for f in range(dim):
        for c in range(dim):
            matriz[f][c]=matriz[c][f]
            matriz[c][f]=matriz[f][c]
    return matriz

def promedioFila(matriz,f,dim):
    """Calcula el promedio de los elementos de una fila,
    cuyo número se recibe como parámetro."""
    f-=1
    suma=0
    for c in range(dim):
        suma=suma+matriz[f][c]
    prom=suma/dim
    return prom

def porcentajeImparesFila(matriz,dim,f):
    """Calcula el porcentaje de elementos con valor impar en una columna,
    cuyo número se recibe como parámetro."""
    f-=1
    cont=0
    for c in range(dim):
        if matriz[f][c]%2!=0:
            cont+=1
    porcentaje=cont/dim*100
    return porcentaje

def filasPalindromos(matriz,dim):
    """Determina qué columnas de la matriz son palíndromos (capicúas), devolviendo
    una lista con los números de las mismas."""
    filasPal=[]
    esPalindromo=True
    for f in range(dim):
        for c in range(dim):
            if matriz[f][c] != matriz[f][-(c+1)]:
                esPalindromo=False
                break
        if esPalindromo==True:
            filasPal.append(f+1)
    return filasPal
        
def imprimirMatriz(matriz):
    """Imprime la matriz"""
    for f in range (dim):
        for c in range (dim):
            print("%3d" %matriz[f][c], end="")
        print( )

#Programa principal
dim=int(input("Ingrese un entero para definir la dimension de la matriz NxN: "))
matriz=crearMatriz(dim)
matriz=cargarMatriz(dim,matriz)
imprimirMatriz(matriz)
ordenarMatriz(dim,matriz)
print("La matriz ordenada es:")
imprimirMatriz(matriz)
f1=int(input("Ingrese la primera fila a intercambiar: "))
f2=int(input("Ingrese la segunda fila a intercambiar: "))
matriz=intercambiarFilas(matriz,dim,f1,f2)
print("La matriz luego del intercambio fila por fila es:")
imprimirMatriz(matriz)
c1=int(input("Ingrese la primera columna a intercambiar: "))
c2=int(input("Ingrese la segunda columna a intercambiar: "))
matriz=intercambiarColumnas(matriz,dim,c1,c2)
print("La matriz luego del intercambio columna por columna es:")
imprimirMatriz(matriz)
f=int(input("Ingrese la fila a intercambiar: "))
c=int(input("Ingrese la columna a intercambiar: "))
matriz=intercambiarFilaCol(matriz,dim,f,c)
print("La matriz luego del intercambio fila por columna es:")
imprimirMatriz(matriz)
matriz=trasponerMatriz(matriz,dim)
print("La matriz traspuesta es:")
imprimirMatriz(matriz)
filaprom=int(input("Ingrese el numero de fila para calcular el promedio: "))
prom=promedioFila(matriz,filaprom,dim)
print("El promedio de la fila %d es: %f" %(filaprom,prom))
filaporc=int(input("Ingrese el numero de fila para calcular porcentaje de impares: "))
porc=porcentajeImparesFila(matriz,dim,filaporc)
print("El porcentaje de impares de la fila %d es: %f%%" %(filaporc,porc))
filaspal=filasPalindromos(matriz,dim)
print("Las filas palíndromos de la matriz son:", filaspal)
