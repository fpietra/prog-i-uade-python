def crear_matriz(dim):
    matriz_base=[]
    for f in range(dim):
        matriz_base.append([0]*dim)
    return matriz_base

def impares_principal(matriz,dim):
    """Genera una matriz con números impares positivos en la diagonal principal"""
    num=1
    for f in range(dim):
        for c in range(dim):
            if f == c:
                matriz[f][c]=num
        num+=2
    return matriz

def cubicos_secundaria(matriz,dim):
    """Genera una matriz con números elevados al cubo en la diagonal secundaria"""
    num=3**(dim-1)
    for f in range(dim):
        for c in range(dim):
            if f+c==dim-1:
                matriz[f][c]=num
        num=3**(dim-(f+2))
    return matriz

def ceros_transprincipal(matriz,dim):
    num=dim
    for f in range(dim):
        for c in range(dim):
            if c <= f:
                matriz[f][c]=num
        num-=1
    return matriz

def numeros_fila(matriz,dim):
    num=dim
    for f in range(dim):
        for c in range(dim):
            matriz[f][c]=num
        num-=1
    return matriz

def ceros_salteados(matriz,dim):
    num=1
    for f in range(dim):
        for c in range(dim):
            if f%2==0 and c%2!=0:
                matriz[f][c]=num
                num+=1
            elif f%2!=0 and c%2==0:
                matriz[f][c]=num
                num+=1
    return matriz

def ascendente_reverso(matriz,dim):
    num=1
    cant=1
    for f in range(dim):
        for c in range(1,dim+1-(dim-f-1)): 
            matriz[f][-c]=num
            num+=1
    return matriz

def imprimir_matriz(matriz):
    """Imprime la matriz"""
    for f in range(dim):
        for c in range(dim):
            print("%5d" %matriz[f][c], end="")
        print ( )

#Programa principal
print("Elija el tipo de matriz a crear:")
print("1.Matriz ascendente con impares positivos en la diagonal principal")
print("2.Matriz descendente con cubos de 3 en la diagonal secundaria")
print("3.Matriz descendente con ceros tras la diagonal principal")
print("4.Matriz descendente con el número de fila")
print("5.Matriz ascendente con ceros intercalados")
print("6.Matriz ascendente inversa por filas")
opcion=int(input("Ingrese la opción deseada: "))
if opcion == 1:
    dim=int(input("Ingrese la cantidad de filas de la matriz: "))
    matriz=crear_matriz(dim)
    result=impares_principal(matriz,dim)
    imprimir_matriz(matriz)
elif opcion == 2:
    dim=int(input("Ingrese la cantidad de filas de la matriz: "))
    matriz=crear_matriz(dim)
    result=cubicos_secundaria(matriz,dim)
    imprimir_matriz(matriz)
elif opcion == 3:
    dim=int(input("Ingrese la cantidad de filas de la matriz: "))
    matriz=crear_matriz(dim)
    result=ceros_transprincipal(matriz,dim)
    imprimir_matriz(matriz)
elif opcion == 4:
    dim=int(input("Ingrese la cantidad de filas de la matriz: "))
    matriz=crear_matriz(dim)
    result=numeros_fila(matriz,dim)
    imprimir_matriz(matriz)
elif opcion == 5:
    dim=int(input("Ingrese la cantidad de filas de la matriz: "))
    matriz=crear_matriz(dim)
    result=ceros_salteados(matriz,dim)
    imprimir_matriz(matriz)
elif opcion == 6:
    dim=int(input("Ingrese la cantidad de filas de la matriz: "))
    matriz=crear_matriz(dim)
    result=ascendente_reverso(matriz,dim)
    imprimir_matriz(matriz)