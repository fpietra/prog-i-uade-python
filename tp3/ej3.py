##Desarrollar un programa para rellenar una matriz de N x N con números enteros al
##azar comprendidos en el intervalo [0,N2), de tal forma que ningún número se repita.
##Imprimir la matriz por pantalla.

import random

def crearMatriz(n):
  filas=columnas=n
  matrizCuad=[]
  for f in range(filas):
    matrizCuad.append([-1]*columnas)
  return matrizCuad

def llenarMatriz(matriz,n):
  numeros=[]
  for f in range(n):
    for c in range(n):
      num=random.randint(0,n**2-1)
      while num in numeros:
        num=random.randint(0,n**2-1)
      matriz[f][c]=num
      numeros.append(num)
  return matriz

def imprimirMatriz(matriz,n):
  for f in range (n):
    for c in range(n):
      print("%3d" %matriz[f][c], end="")
    print ( )

n=int(input("Ingrese la cantidad de filas y columnas que debe tener la matriz cuadrada: "))
matriz=crearMatriz(n)
matriz=llenarMatriz(matriz,n)
imprimirMatriz(matriz,n)