"""Desarrollar una función que reciba tres números enteros positivos y verifique si corresponden a una fecha gregoriana válida (día, mes, año). Devolver True o False según la
fecha sea correcta o no. Realizar también un programa para verificar el comportamiento de la función."""

def fechaValida (d,m,a):
    """Comprueba si la fecha ingresada es válida en el calendario gregoriano"""
    valida = False
    if 0 < m and m < 13:
        if m == 1 or m == 3 or m == 5 or m == 7 or m == 8 or m == 10 or m == 12:
            if 0 < d and d < 32:
                valida = True
        elif m == 4 or m == 6 or m == 9 or m == 9 or m == 11:
            if 0 < d and d < 31:
                valida = True
        elif a % 4 == 0 and a % 100 != 0 or a % 400 == 0 and m == 2:
            if 0 < d and d < 30:
                valida = True
        elif m == 2 and 0 < d and d < 29:
            valida = True
    return valida

dia = int(input("Ingrese el número de día: "))
mes = int(input("Ingrese el número de mes: "))
anio = int(input("Ingrese el número de año: "))
fVal = fechaValida(dia,mes,anio)
if fVal==True:
    print ("La fecha ingresada es válida en el calendario gregoriano")
else:
    print ("La fecha ingresada no es válida en el calendario gregoriano")
