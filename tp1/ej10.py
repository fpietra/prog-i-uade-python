def diadelasemana(dia,mes,año):
    if mes<3:
        mes=mes+10
        año=año-1
    else:
        mes=mes-2
    siglo=año//100
    año2=año%100
    diasem=(((26*mes-2)//10)+dia+año2+(año2//4)+(siglo//4)-(2*siglo))%7
    if diasem<0:
        diasem=diasem+7
    return diasem

#Programa principal
dia=1
mes=int(input("Ingrese el mes: "))
anio=int(input("Ingrese el año: "))
print ("D   L   M   M   J   V   S")
dsem=diadelasemana(dia,mes,anio)
esp=dsem*4-1
if dsem == 6:
  print(" "*esp,dia)
else:
  print(" "*esp,dia,end="")
while dia < 31:
  dia+=1
  dsem=diadelasemana(dia,mes,anio)
  if dia>9:
    if dsem== 6:
      print(" ",dia)
    elif dsem == 0:
      print(dia, end="")
    else:
      print(" ",dia, end="")#Imprime espacios hasta alinear la fecha con el día que le corresponde
  else:
    if dsem== 6:
      print(" "*2,dia)
    elif dsem == 0:
      print(dia, end="")
    else:
      print(" "*2,dia, end="")#Imprime espacios hasta alinear la fecha con el día que le corresponde
