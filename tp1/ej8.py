"""Escribir una función que reciba como parámetro número del 1 al 9 y devuelva el resultado de sumar n + nn + nnn + nnnn, donde n corresponde al número recibido.
Por ejemplo, si se ingresa 3 debe devolver 3702 (3+33+333+3333). Escribir también un programa para verificar el comportamiento de la función."""

def suma(n):
    result=n
    previo=n
    result=result+(n*10+previo)
    previo=n*10+previo
    result=result+(n*100+previo)
    previo=n*100+previo
    result=result+(n*1000+previo)
    return result

num=int(input("Ingrese un número entero entre 1 y 9: "))
res=suma(num)
print("El resultado es:", res)
