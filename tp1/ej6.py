"""Escribir funciones para imprimir por pantalla los siguientes patrones de asteriscos,donde la cantidad de filas se recibe como parámetro:
**********                **
**********                ****
**********                ******
**********                ********
**********                **********

"""

def impCuad(cant):
    for i in range(cant):
        print("**********")

def impEsc(cant):
    ast=1
    while cant != 0:
        for i in range(ast*2):
            print("*", end="")
        cant-=1
        ast+=1
        print(" ")
    
tipo = int(input("Ingrese el tipo de patrón a imprimir: 1 para líneas iguales, 2 para escalera): "))
cant = int(input("Ingrese la cantidad de filas que desea imprimir: "))
if tipo == 1:
    impCuad(cant)
elif tipo == 2:
    impEsc(cant)
