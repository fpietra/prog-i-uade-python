"""Escribir una función diasiguiente(…) que reciba como parámetro una fecha cualquiera expresada por tres enteros (correspondientes al día, mes y año) y calcule y
devuelva tres enteros correspondientes el día siguiente al dado.
Utilizando esta función, desarrollar programas que permitan:
a. Sumar N días a una fecha.
b. Calcular la cantidad de días existentes entre dos fechas cualesquiera."""

def diasiguiente(d,m,a):
    if m == 1 or m == 3 or m == 5 or m == 7 or m == 8 or m == 10:
        if d == 31:
            d=1
            m+=1
        else:
            d+=1
    elif m == 12:
        if d == 31:
            d=1
            m=1
            a+=1
        else:
            d+=1
    elif m == 4 or m == 6 or m == 9 or m == 11:
        if d == 30:
            d=1
            m+=1
        else:
            d+=1
    elif m == 2 and (a % 4 == 0 and a % 100 != 0 or a % 400 == 0):
        if d == 29:
            d=1
            m+=1
        else:
            d+=1
    elif m == 2:
        if d == 28:
            d=1
            m+=1
        else:
            d+=1
    return(d,m,a)

def fechaEntera(d,m,a):
    """Convierte una fecha d/m/aaaa en un entero aaaammdd"""
    fent=a*10000+m*100+d
    return fent

dia = int(input("Ingrese el número de día: "))
mes = int(input("Ingrese el número de mes: "))
anio = int(input("Ingrese el número de año: "))
opcion = int(input("Elija una opción\n(1)Obtener la fecha siguiente\n(2)Obtener la fecha dentro de N días\n(3)Calcular candidad de días con otra fecha\nIngrese la opción desada: "))
if opcion == 1:
    dsig, msig, asig=diasiguiente(dia,mes,anio)
    print ("El día siguiente al ingresado es:", dsig,"/", msig,"/", asig,sep=""  )
elif opcion == 2:
    dsig, msig, asig=diasiguiente(dia,mes,anio)
    cantDias=int(input("Ingrese la cantidad de días que quiere sumar a la fecha ingresada: "))
    for i in range(1,cantDias):
        dsig, msig, asig=diasiguiente(dsig, msig, asig)
    print ("La nueva fecha es:", dsig,"/", msig,"/", asig,sep="")
elif opcion == 3:
    print("AVISO: La fecha deberá ser posterior a la ingresada previamente")
    contador = 0
    dia2 = int(input("Ingrese el segundo número de día: "))
    mes2 = int(input("Ingrese el segundo número de mes: "))
    anio2 = int(input("Ingrese el segundo número de año: "))
    fecha1=fechaEntera(dia,mes,anio)
    fecha2=fechaEntera(dia2,mes2,anio2)
    while fecha1<fecha2:
        dia, mes, anio=diasiguiente(dia, mes, anio)
        fecha1=fechaEntera(dia, mes, anio)
        contador+=1
    print("Entre %d/%d/%d y %d/%d/%d hay %d días" %(dia,mes,anio,dia2,mes2,anio2,contador))


