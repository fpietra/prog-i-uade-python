"""Un comercio de electrodomésticos necesita para su línea de cajas un programa que le indique al cajero el cambio que debe entregarle al cliente. Para eso se ingresan
dos números enteros, correspondientes al total de la compra y al dinero recibido.
Informar cuántos billetes de cada denominación deben ser entregados al cliente como vuelto, de tal forma que se minimice la cantidad de billetes. Considerar que
existen billetes de $500, $100, $50, $20, $10, $5 y $1. Emitir un mensaje de error si el dinero recibido fuera insuficiente. Ejemplo: Si la compra es de $317 y se abona
con $500, el vuelto debe contener 1 billete de $100, 1 billete de $50, 1 billete de $20, 1 billete de $10 y 3 billetes de $1."""

def darVuelto(recibo, precio):
    b500 = 0
    b100 = 0
    b50 = 0
    b20 = 0
    b10 = 0
    b5 = 0
    b1 = 0
    vuelto = recibo - precio
    while vuelto >= 500:
        b500 += 1
        vuelto -= 500
    while vuelto >= 100:
        b100 += 1
        vuelto -= 100
    while vuelto >= 50:
        b50 += 1
        vuelto -= 50
    while vuelto >= 20:
        b20 += 1
        vuelto -= 20
    while vuelto >= 10:
        b10 += 1
        vuelto -= 10
    while vuelto >= 5:
        b5 += 1
        vuelto -= 5
    while vuelto >= 1:
        b1 += 1
        vuelto -= 1            
    return (b500, b100, b50, b20, b10, b5, b1)

recibo=int(input("Ingrese el monto recibido: "))
precio=int(input("Ingrese el precio del producto: "))
if recibo>=precio:
    b500, b100, b50, b20, b10, b5, b1 = darVuelto(recibo,precio)
    print ("Vuelto:\n")
    print ("%d billetes de $500\n%d billetes de $100\n%d billetes de $50\n%d billetes de $20\n%d billetes de $10\n%d billetes de $5\n%d billetes de $1" %(b500, b100, b50,
                                                                                                                                                         b20, b10, b5, b1))
else:
    print ("El dinero recibido es insuficiente para realizar esta compra")
