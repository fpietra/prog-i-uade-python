def calcularCosto (cantidadViajes):
  costo = 0
  if cantidadViajes<20:
    costo=cantidadViajes*12.5
  elif cantidadViajes<30:
    costo=12.5*20+10*(cantidadViajes-20)
  elif cantidadViajes<40:
    costo=12.5*20+10*10+8.75*(cantidadViajes-30)
  elif cantidadViajes>40:
    costo=12.5*20+10*10+8.75*10+7.5*(cantidadViajes-40)
  return costo

cantidadViajes = int(input("Ingrese la cantidad de viajes: "))
total=calcularCosto(cantidadViajes)
print ("El costo total es $", total)