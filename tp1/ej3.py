"""Para un número entero N menor de 100 recibido como parámetro, escribir un programa que utilice una función para devolver la suma de los cuadrados de aquellos números
entre 1 y N que están separados entre si por cuatro unidades."""

def cuadCuatro(n):
    result = 0
    for i in range(1, n+1,4):
        result=result+i**2
    return result

num=int(input("Ingrese un número entero menor a 100: "))
res=cuadCuatro(num)
print("El resultado es: ", res)