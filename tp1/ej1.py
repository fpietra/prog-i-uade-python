"""Desarrollar una función que reciba tres números positivos y devuelva el mayor de los tres, sólo si éste es único (mayor estricto). En caso de no existir el mayor estricto
devolver -1. No utilizar operadores lógicos (and, or, not). Desarrollar también un programa para ingresar los tres valores, invocar a la función y mostrar el
máximo hallado, o un mensaje informativo si éste no existe."""

def mayor(a, b, c):
    numMayor=0
    if a > b:
        if a > c:
            numMayor = a
        if a < c:
            numMayor = c
        else:
            numMayor = -1
    elif b > a:
        if b > c:
            numMayor = b
        if b < c:
            numMayor = c
        else:
            numMayor = -1
    elif c > a:
        if c > b:
            numMayor = c
        if c < b:
            numMayor = b
        else:
            numMayor = -1
    else:
        numMayor = -1
    return numMayor

a = int(input("Ingrese el primer número entero positivo: "))
b = int(input("Ingrese el segundo número entero positivo: "))
c = int(input("Ingrese el tercer número entero positivo: "))
mayornum = mayor (a, b, c)
print ("El mayor número ingresado es:", mayornum)
