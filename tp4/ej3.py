##Escribir un programa que permita ingresar una cadena de caracteres y coloque en
##mayúscula la primera letra posterior a un espacio, eliminando todos los espacios
##que contenga. Imprimir por pantalla la cadena obtenida.

def camello(cad):
    cad=cad.title()
    palabras=cad.split()
    cad_res=""
    for i in range (len(palabras)):
        cad_res=cad_res+palabras[i]
    return cad_res

#Programa principal
cadena=input("Ingrese una frase: ")
cadena=camello(cadena)
print(cadena)
