##Desarrollar una función que extraiga una subcadena de una cadena de caracteres, indicando la posición y la cantidad de caracteres deseada. Devolver la subcadena como valor de retorno.
##Escribir también un programa para verificar el comportamiento de la misma. Ejemplo, dada la cadena "El número de teléfono es 43567890" extraer la subcadena que comienza en la posición 25 y tiene 9 caracteres,
##resultando la subcadena "4356-7890". Escribir una función para cada uno de los siguientes casos: a.Utilizando rebanadas b.Sin utilizar rebanadas

def subcadena_reb(cad, pos, car):
    subcad=cad[pos:pos+car]
    return subcad

def subcadena_no_reb(cad,pos,car):
    subcad=cad[pos]
    for i in range(pos+1,pos+car):
        subcad=subcad+cad[i]
    return subcad

#Programa principal
cadena=input("Ingrese una frase: ")
pos=int(input("Ingrese la posicion incial de la subcadena: "))
hasta=int(input("Ingrese la cantidad de caracteres a extraer: "))
sub_reb=subcadena_reb(cadena,pos,hasta)
sub_no_reb=subcadena_no_reb(cadena,pos,hasta)
print(sub_reb)
print(sub_no_reb)
