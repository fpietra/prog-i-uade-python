##Escribir una función para eliminar una subcadena de una cadena de caracteres, a
##partir de una posición y cantidad de caracteres dadas, devolviendo la cadena resultante.
##Escribir también un programa para verificar el comportamiento de la misma.
##Escribir una función para cada uno de los siguientes casos:
##a. Utilizando rebanadas
##b. Sin utilizar rebanadas

def eliminar_no_reb(cad,inicio,cant):
    subcad=""
    for i in range(inicio):
        subcad=subcad+cad[i]
    for j in range(len(cad)-(inicio+cant)):
        subcad=subcad+cad[j+inicio+cant]
    return subcad

def eliminar_reb(cad,inicio,cant):
    subcad=cad[:inicio]+cad[inicio+cant:]
    return subcad

#Programa principal
cadena=input("Ingrese una frase: ")
inicio=int(input("Ingrese la posición inicial a eliminar: "))
cant=int(input("Ingrese la cantidad de caracteres a eliminar: "))
res=eliminar_no_reb(cadena,inicio,cant)
res2=eliminar_reb(cadena,inicio,cant)
print(res)
print(res2)