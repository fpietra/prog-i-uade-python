##Desarrollar una función que determine si una cadena de caracteres es capicúa, sin utilizar cadenas auxiliares. Escribir además un programa que permita verificar su funcionamiento.

def cadena_capicua(cad):
    capicua=True
    for i in range (len(cad)):
        if cad[i]!=cad[-(i+1)]:
            capicua=False
            break
    return capicua

#Programa principal
cad=input("Ingrese una frase:")
capi=cadena_capicua(cad)
print("La cadena es capicua:", capi)