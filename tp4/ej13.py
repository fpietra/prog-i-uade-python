##Escribir una función que reciba como parámetro una cadena de caracteres en la
##que las palabras se encuentran separadas por uno o más espacios. Devolver otra
##cadena con las palabras ordenadas alfabéticamente, dejando un espacio entre cada
##una.

def cadena_lista(cad):
    """Convierte una cadena en una lista de palabras"""
    palabras=cad.split()
    return palabras

def ordenar_palabras(cad):
    """Ordena alfabéticamente las palabras mediante burbujeo"""
    palabras=cadena_lista(cad)
    for vuelta in range(len(palabras)-1,0,-1):
        for indice in range(vuelta):
            if palabras[indice]>palabras[indice+1]:
                palabras[indice],palabras[indice+1]=palabras[indice+1],palabras[indice]
    return palabras

def lista_cadena(lst):
    """Convierte una lista en una cadena"""
    cad_final=""
    for i in range(len(lst)):
        cad_final=cad_final+lst[i]
        cad_final=cad_final+" "
    return cad_final

#Programa principal
cadena=input("Ingrese una frase: ")
lista_palabras=ordenar_palabras(cadena)
result=lista_cadena(lista_palabras)
print(result)