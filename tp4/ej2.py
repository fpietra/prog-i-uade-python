##Leer una cadena de caracteres e imprimirla centrada en pantalla. Suponer que la misma tiene 80 columnas.

def centrar(cad):
    esp=40-len(cad)//2
    cad_esp=" "*esp
    cad_final=cad_esp+cad
    return cad_final

#Programa principal
cad=input("Ingrese una cadena: ")
cadfinal=centrar(cad)
print(cadfinal)
    
