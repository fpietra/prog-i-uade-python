##Los números de claves de dos cajas fuertes están intercalados dentro de un número
##entero llamado "clave maestra". Realizar un programa para obtener ambas claves,
##donde la primera se construye con los dígitos impares de la clave maestra y la
##segunda con los dígitos pares. Los dígitos se numeran desde la izquierda. Ejemplo:
##Si clave maestra = 12345, la clave 1 sería 135 y la clave 2 sería 24.

def obtener_claves(maestra):
    clave1=maestra[::2]
    clave2=maestra[1::2]
    return clave1, clave2

#Programa principal
clave_maestra=input("Ingrese la clave maestra: ")
clave1, clave2=obtener_claves(clave_maestra)
print("La clave 1 es:", clave1)
print("La clave 2 es:", clave2)
