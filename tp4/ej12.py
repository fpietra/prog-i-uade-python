##Escribir un programa que permita ingresar una cadena de caracteres e imprima un
##mensaje indicando cuántas letras y cuántos números contiene. Por ejemplo, si se
##ingresa "Hola Mundo 123" debe indicar que se ingresaron 9 letras y 3 números.


def letras_numeros(cad):
    cant_numeros=0
    cant_letras=0
    for i in range(len(cad)):
        if cad[i].isalpha():
            cant_letras+=1
        if cad[i].isdigit():
            cant_numeros+=1
    return(cant_numeros, cant_letras)

#Programa principal
cadena=input("Ingrese una frase: ")
cn, cl=letras_numeros(cadena)
print("En la cadena hay {} letras y {} números".format(cl, cn))
