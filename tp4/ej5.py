##Escribir una función que reciba como parámetro un número entero entre 0 y 999 y lo convierta en un número romano, devolviéndolo en una cadena de caracteres. ¿En qué cambiaría la función si el rango de valores fuese diferente?

def c_d_u(num):
    u=num%10
    num=num//10
    d=num%10
    num=num//10
    c=num
    return (u,d,c)

def arabe_romano(num):
    u,d,c=c_d_u(num)
    if u > 0:
        if u>0 and u<4:
            cad_uni="I"*u
        elif u==4:
            cad_uni="IV"
        elif u==5:
            cad_uni="V"
        elif u>5 and u < 9:
            cad_uni="V"+"I"*(u-5)
        elif u==9:
            cad_uni="IX"
        ara_rom=cad_uni
    if d > 0:
        if d>0 and d<4:
            cad_dec="X"*d
        elif d==4:
            cad_dec="XL"
        elif d==5:
            cad_dec="L"
        elif d>5 and d < 9:
            cad_dec="L"+"X"*(d-5)
        elif d==9:
            cad_dec="XC"
        ara_rom=cad_dec+cad_uni
    if c > 0:
        if c>0 and c<4:
            cad_cen="C"*c
        elif c==4:
            cad_cen="CD"
        elif c==5:
            cad_cen="D"
        elif c>5 and c < 9:
            cad_cen="D"+"C"*(c-5)
        elif c==9:
            cad_cen="CM"
        ara_rom=cad_cen+cad_dec+cad_uni
    if num==0:
        ara_rom="No es posible representar el numero"
    return ara_rom

#Programa principal
num=int(input("Ingrese un numero entero entre 0 y 999: "))
num_rom=arabe_romano(num)
print("El numero romano es:", num_rom)
