##Desarrollar una función para reemplazar todas las apariciones de una palabra por
##otra en una cadena de caracteres y devolver la cadena obtenida. Escribir también
##un programa para verificar el comportamiento de la función.

def cambiar(cad,original,nueva):
    buscar=" "+original+" "
    reem=" "+nueva+" "
    nueva=cad.replace(buscar,reem)
    return nueva

#Programa principal
cadena=input("Ingrese una frase: ")
orig=input("Buscar: ")
otra=input("Reemplazar con: ")
reemplazada=cambiar(cadena,orig,otra)
print(reemplazada)
