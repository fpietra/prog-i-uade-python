##Escribir un programa que cuente cuántas veces se encuentra una subcadena dentro
##de otra cadena, sin diferenciar mayúsculas y minúsculas. Tener en cuenta que
##los caracteres de la subcadena no necesariamente deben estar en forma consecutiva
##dentro de la cadena, pero sí respetando el orden de los mismos.
##@author: naza

def contador(cad,subcad):
    cad=cad.upper()
    subcad=subcad.upper()
    contador=0
    j=0
    cant_subcad=len(subcad)
    ctrl_subcad=0
    for i in range(len(cad)):
        if cad[i]==subcad[j]:
            ctrl_subcad+=1
            j+=1
            if ctrl_subcad==cant_subcad:
                ctrl_subcad=0
                j=0
                contador+=1
    return contador

#Programa principal
cadena=input("Ingrese una frase: ")
subcadena=input("Ingrese la cadena a buscar: ")
result=contador(cadena,subcadena)
print(result)
