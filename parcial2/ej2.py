##Una secuencia hailstone es una secuencia terminada en 1 que se genera a partir de un número cualquiera en el que el siguiente se
##divide por dos si es par y se multiplica por tres y se le suma uno si es impar.
##Ej: 5; 16, 8, 4, 2, 1
##Escribir una función recursiva para generar al secuencia e imprimirla por pantalla.

def hailstone(num):
    if num == 1:
        print(num)
        return 1
    elif num % 2 != 0:
        print(num,end=" ")
        return(hailstone(num*3+1))
    elif num % 2 == 0:
        print(num,end=" ")
        return(hailstone(num//2))

#Programa principal
while True:
    try:
        num=int(input("Ingrese un número entero mayor a 1: "))
        assert (num>1), "El número debe ser mayor a 1."
        hailstone(num)
        break
    except ValueError:
        print("El número debe ser entero.")
    except AssertionError as mensaje:
        print (mensaje)
