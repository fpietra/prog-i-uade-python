def numero_lista(clave_junta):
    lista_original=[]
    for i in range(8): #15263748
        num=clave_junta%10
        clave_junta=clave_junta//10
        lista_original.append(num)
    lista_original.reverse( )
    return lista_original

def dividir(lista):
    clave1=[]
    clave2=[]
    for i in range(8):
        if i%2!=0:
            clave1.append(lista[i])
        else:
            clave2.append(lista[i])
    return (clave1,clave2)

#Programa principal
clave=int(input("Ingrese la clave: "))
lista=numero_lista(clave)
clave1, clave2=dividir(lista)
print("Clave original:", lista)
print("Clave1: ",clave1)
print("Clave2: ",clave2)