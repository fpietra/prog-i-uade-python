##La raíz cuadrada de un número puede obtenerse mediante la función sqrt() del módulo math.
##Escribir un programa que utilice esta función para calcular la raíz cuadrada de un número cualquiera ingresado a través del teclado.
##El programa debe utilizar manejo de excepciones para evitar errores si se ingresa un número negativo. 

import math

try:
    num=float(input("Ingrese un numero: "))
    sq=math.sqrt(num)
    print("La raiz cuadrada es:", sq)
except ValueError:
    print("No se puede calcular la raiz cuadrada de un numero negativo")