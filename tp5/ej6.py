##Escribir un programa que juegue con el usuario a adivinar un número. El programa debe generar un número al azar entre 1 y 500 y el usuario debe adivinarlo. Para eso, cada vez que se introduce un valor se muestra
##un mensaje indicando si el número que tiene que adivinar es mayor o menor que el ingresado. Cuando consiga adivinarlo, se debe imprimir en pantalla la cantidad de intentos que le tomó hallar el número.
##Si el usuario introduce algo que no sea un número se mostrará un mensaje en pantalla y se lo contará como un intento más. 

import random

def adivinar(sec):
    turnos=1
    while True:
        try:
            intento=int(input("Adivine el numero secreto: "))
            while intento != sec:
                if intento > sec:
                    print ("El numero es menor que el ingresado")
                    turnos+=1
                if intento < sec:
                    print("El numero es mayor que el ingresado")
                    turnos+=1
                intento=int(input("Adivine el numero secreto: "))
            break
        except ValueError:
            print("Debe ingresar numeros solamente.")
            turnos+=1
    return turnos

#Programa principal
secreto=random.randint(1,500)
turnos=adivinar(secreto)
print("FELICITACIONES. Ha adivinado el numero en {} intentos".format(turnos))
