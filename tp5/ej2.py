##Desarrollar una función que devuelva una cadena de caracteres con el nombre del mes cuyo número se recibe como parámetro. Los nombres de los meses deberán obtenerse de
##una lista de cadenas de caracteres inicializada dentro de la función.
##Devolver una cadena vacía si el número de mes es inválido. La detección de meses inválidos deberá realizarse a través de excepciones
def num_mes(num):
    try:
        meses=["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
        assert 1<=num<=12
        return meses[num-1]
    except AssertionError:
        meses_e=""
        return meses_e

#Programa principal
num=int(input("Ingrese el numero del mes: "))
mes=num_mes(num)
print(mes)
