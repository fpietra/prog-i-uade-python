##El método index permite buscar un elemento dentro de una lista, devolviendo la posición que éste ocupa. Sin embargo, si el elemento no pertenece a la lista se produce una excepción de tipo ValueError.
##Desarrollar un programa que cargue una lista con elementos y permita que el usuario busque la posición que ocupa alguno de ellos utilizando el método index.
##Si el elemento no pertenece a la lista se imprimirá un mensaje de error y se solicitará un nuevo elemento para buscar. No utilizar el operador in

import random

def crear_lista():
    cant=random.randint(10,300)
    lista=[]
    for i in range (cant):
        num=random.randint(0,500)
        lista.append(num)
    return lista

def buscar(lista):
    while True:
        try:
            numero=int(input("Ingrese el numero a buscar: "))
            indice=lista.index(numero)
            return indice
            break
        except ValueError:
            print ("El numero no esta en la lista. Intente nuevamente")
    

#Programa principal
lista=crear_lista()
print(lista)
indice=buscar(lista)
print("El numero esta en la posicion {}".format(indice))