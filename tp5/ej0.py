def entero_mayor(mensaje="Ingrese un número entero mayor a 0: "):
    while True:
        try:
            n=float(input(mensaje))
            assert n == int(n), "Debe ser entero"
            assert n > 0, "Debe ser mayor a 0"
            break
        except ValueError:
            print("Debe ser un número")
        except AssertionError as MensajeError:
            print(MensajeError)
    return int (n)

#Programa principal

num=entero_mayor()
print(num)
