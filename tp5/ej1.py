##Realizar una función que reciba como parámetros dos cadenas de caracteres conteniendo números reales, sume ambos valores y devuelva el resultado como un número real.
##Devolver -1 si alguna de las cadenas no contiene un número válido, utilizando manejo de excepciones para detectar el error.

def suma_reales(cad1,cad2):
    try:
        suma_r=float(cad1)+float(cad2)
        return suma_r
    except ValueError:
        suma_r=-1
        return suma_r

#Programa principal
op1=input("Ingrese el operador 1 (numero real): ")
op2=input("Ingrese el operador 2 (numero real): ")
resultado=suma_reales(op1,op2)
print("La suma es", resultado)
