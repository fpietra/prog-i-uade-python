##Todo programa Python es susceptible de ser interrumpido mediante
##la pulsación de las teclas Ctrl-C, lo que genera una excepción del tipo KeyboardInterrupt.
##Realizar un programa para imprimir los números enteros entre 1 y 100000 evitando que pueda ser interrumpido.

for i in range(10000):
    try:
        print(i+1)
    except KeyboardInterrupt:
        pass
