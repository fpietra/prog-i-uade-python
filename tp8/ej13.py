##Una librería almacena su lista de precios en un diccionario. Diseñar un programa para crearlo, incrementar los precios de los cuadernos en un 15%,
##imprimir un listado con todos los elementos de la lista de precios e indicar cuál es el ítem más costoso que venden en el comercio.

productos={}
mayor_precio=0
producto=input("Ingrese el nombre del producto o Enter para terminar: ")
while producto !="":
    precio=float(input("Ingrese el precio del producto: ")) #Asegúrese de ingresar el producto "Cuaderno"
    productos[producto]=precio
    producto=input("Ingrese el nombre del producto o Enter para terminar: ")

print(productos)
productos["Cuaderno"]=productos["Cuaderno"]*1.15
for prod in productos:
    print(prod,": ${:.2f}".format(productos[prod]))
    
for prod in productos:
    if productos[prod]>mayor_precio:
        prod_mayor=prod
        mayor_precio=productos[prod]
print("El producto {} tiene el mayor precio {:.2f}: $".format(prod_mayor,mayor_precio))