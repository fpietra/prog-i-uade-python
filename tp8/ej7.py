##Ingresar una frase desde el teclado y eliminar las palabras repetidas,
##dejando un solo ejemplar de cada una. Finalmente mostrar las palabras ordenadas alfabéticamente.
##La eliminación de las palabras duplicadas debe realizarse a través de un conjunto.

conj=set()
frase=input("Ingrese una frase: ")
frase=frase.rstrip(".").lower()
lfrase=frase.split(" ")
for i in range(len(lfrase)):
    conj.add(lfrase[i])
conj_ord=sorted(conj)
print(conj_ord)
