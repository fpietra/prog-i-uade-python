##Escribir una función buscarclave() que reciba como parámetros un diccionario y un valor, y devuelva una lista de claves que apunten
##("mapeen") a ese valor en el diccionario. Comprobar el comportamiento de la función mediante un programa apropiado.

def buscarclave(dic, valor):
    claves=[]
    for clave in dic:
        if dic[clave]==valor:
            claves.append(clave)
    print("Las claves que apuntan al valor son: ",end="")
    for i in range(len(claves)):
        print(claves[i], end=" ")

#Programa principal
dic={1:2,2:3,3:4,4:5,5:2}
buscar=int(input("Ingrese el valor para hallar las correspondientes claves: ")) #Ingresar 5
buscarclave(dic,buscar)
