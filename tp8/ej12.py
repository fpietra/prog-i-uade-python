##Crear una función contar_vocales(), que reciba una palabra y cuente cuántas letras "a" contiene, cuántas "e", cuántas "i", etc. Devolver un diccionario con los resultados.
##Desarrollar un programa para leer una frase e invocar a la función por cada palabra que contenga la misma. Imprimir cada palabra y la cantidad de vocales hallada.

def contar_vocales(pal):
    a=e=i=o=u=0
    vocales={}
    for letra in pal:
        if letra == "a" or letra=="á":
            a+=1
            vocales["a"]=a
        elif letra =="e" or letra=="é":
            e+=1
            vocales["e"]=e
        elif letra =="i" or letra=="í":
            i+=1
            vocales["i"]=i
        elif letra =="o" or letra=="ó":
            o+=1
            vocales["o"]=o
        elif letra =="u" or letra=="ú" or letra=="ü":
            u+=1
            vocales["u"]=u
    return(vocales)

#Programa principal
frase=input("Ingrese una frase: ")
frase=frase.lower().rstrip(".")
palabras=frase.split(" ")
for palabra in palabras:
    vocales=contar_vocales(palabra)
    print("La palabra '{}' tiene las siguientes vocales {}".format(palabra,vocales))
