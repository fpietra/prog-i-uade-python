##Definir un conjunto con números enteros entre 0 y 9. Luego solicitar valores al usuario y eliminarlos del conjunto mediante el método remove, mostrando el contenido del
##conjunto luego de cada eliminación. Finalizar el proceso al ingresar -1. Utilizar manejo de excepciones para evitar errores al intentar quitar elementos inexistentes.

conj={0,1,2,3,4,5,6,7,8,9}
rem=int(input("Ingrese un entero positivo para removerlo del conjunto: "))

while rem !=-1:
    try:
        conj.remove(rem)
        print(conj)
        rem=int(input("Ingrese un entero positivo para removerlo del conjunto o -1 para terminar: "))
    except KeyError:
        print("El elemento no se encuentra en el conjunto y no puede eliminarse")
        rem=-1