##Generar e imprimir un diccionario donde las claves sean números enteros entre 1 y 20 (ambos incluidos) y los valores asociados sean el cuadrado de las claves.

dic={}
for i in range(1,21):
    dic[i]=i*i

print(dic)
