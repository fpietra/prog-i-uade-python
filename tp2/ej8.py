##Generar dos listas con M y N números al azar entre 1 y 50 y construir una tercera lista cuyos elementos sean el 
##resultado de la intersección de las dos listas dadas. Los valores de M y N se obtienen al azar.
##Imprimir las tres listas por pantalla

import random

def interseccion(lista1,lista2):
    listaInter=[]
    for i in range(len(lista1)):
        if lista1[i] in lista2:
            num=lista1[i]
            listaInter.append(num)
    return listaInter

    
lista1=[]
lista2=[]
m=int(input("Ingrese el largo de la lista 1: "))
n=int(input("Ingrese el largo de la lista 2: "))
for i in range(m):
    num=random.randint(1,50)
    lista1.append(num)
for i in range(n):
    num=random.randint(1,50)
    lista2.append(num)
listaResult=interseccion(lista1,lista2)
print("Lista original 1: ", lista1)
print("Lista original 2: ", lista2)
print("Lista interseccion: ", listaResult)