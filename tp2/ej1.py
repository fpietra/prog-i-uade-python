""". Desarrollar cada una de las siguientes funciones y escribir un programa que permita
verificar su funcionamiento imprimiendo la lista luego de invocar a cada función:
a. Cargar un vector con números al azar de cuatro dígitos. La cantidad de elementos
también será un número al azar de dos dígitos.
b. Calcular y devolver la sumatoria de todos los elementos de la lista anterior.
c. Eliminar todas las repeticiones de un valor en la lista anterior. El valor a eliminar
se recibe como parámetro.
d. Determinar si el contenido de una lista cualquiera es capicúa, sin usar listas
auxiliares."""

import random
def cargarVector(lista):
  cantidad=random.randint(10,99)
  vector=lista
  for i in range(cantidad):
    num=random.randint(1000,9999)
    vector.append(num)
  return vector

def sumatoria(lista):
  suma=sum(lista)
  return suma

def eliminarValor(lista,valor):
  while valor in lista:
    lista.remove(valor)
  return lista

def capicua(lista):
  cap=False
  tam=len(lista)
  x=0
  medio=tam//2
  for i in range(medio):
    if lista[i]==lista[tam-1-i]:
      x=x+1
  if x==i-1:
    cap=True
  return cap

lista=[]
cargarVector(lista)
print (lista)
print ("La sumatoria de los elementos de la lista es:", sumatoria(lista))
valor=int(input("Ingrese el valor a eliminar de la lista: "))
lista=eliminarValor(lista,valor)
print(lista)
print("La lista es capicua:", capicua(lista))
