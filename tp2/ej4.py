"""Definir una función superposición() que reciba como parámetros dos listas de cualquier tipo y devuelva True si tienen al menos un elemento en común, o False en
caso contrario. Desarrollar un programa para verificar su comportamiento."""

def superposicion(lista1,lista2):
    sup=False
    for i in range (len(lista1)):
        if lista1[i] in lista2:
            sup=True
            break
    return sup

vector1=[2,6,8,9,28,103,2056,3]
vector2=[5,7,3]
sup=superposicion(vector1,vector2)
print(sup)