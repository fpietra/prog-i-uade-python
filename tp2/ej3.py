"""Crear una lista con los cuadrados de los números entre 1 y N (ambos incluidos), donde N se ingresa desde el teclado. Luego se solicita imprimir los últimos 10 valores
de la lista, utilizando la técnica de las rebanandas."""

def listaCuad(lista, n):
    """Llena la lista con cuadrados de los números entre 1 y N"""
    vectorCuad=lista
    for i in range (n):
        num=(i+1)**2
        vectorCuad.append(num)
    return vectorCuad

lista=[]
n=int(input("Ingrese un número entero mayor o igual a 1: "))
listaCuad(lista,n)
print("Los cuadrados entre 1 y %d son:" %(n))
print(lista)
print( )
print("Los últimos 10 valores de la lista son: ")
print(lista[n-10:n+1])