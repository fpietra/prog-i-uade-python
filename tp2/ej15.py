##Generar una lista con números al azar entre 1 y 100 y crear una nueva lista con
##los elementos de la primera que sean impares. El proceso deberá realizarse utilizando
##listas por comprensión. Imprimir las dos listas por pantalla.

import random

lista_azar=[]
for i in range(50):
    num=random.randint(1,100)
    lista_azar.append(num)
lista_impares=[lista_azar[i] for i in range(50)if lista_azar[i]%2!=0]
print("Lista original:", lista_azar)
print( )
print("Impares de la lista original:", lista_impares)
