"""Escribir funciones para:
a.Recibir una lista como parámetro y devolver True si la misma contiene algún elemento repetido. La función no debe modificar la lista. 
b.Generar una lista de 50 números aleatorios del 1 al 100 y comprobar con la función anterior si existen elementos duplicados. Devolver True o False.
c.Recibir una lista como parámetro y devolver una nueva lista con los elementos únicos de la lista original, sin importar el orden. 
Combinar estas tres funciones en un mismo programa."""

import random

def repetidos(lista):
    rep=False
    for i in range(len(lista)):
        if lista.count(lista[i])>1:
            rep=True
            break
    return rep

def listaAzar():
    listaAzar=[]
    for i in range(50):
        num=random.randint(1,100)
        listaAzar.append(num)
    return listaAzar

def listaUnicos(lista):
    listaUnicos=[]
    for i in range(len(lista)):
        if lista.count(lista[i])==1:
            listaUnicos.append(lista[i])
    return listaUnicos

lista=[]
listaUni=[]
lista=listaAzar()
rep=repetidos(lista)
print(lista)
print("En la lista hay numeros repetidos:", rep)
listaUni=listaUnicos(lista)
print("La lista de numeros unicos es:", listaUni)
