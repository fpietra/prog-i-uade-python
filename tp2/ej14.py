##Repetir el ejercicio anterior, pero utilizando la técnica de listas por comprensión.
##Escribir un programa para generar una lista con los múltiplos de 7 que no sean múltiplos de 5, entre 2000 y 3500. Imprimir la lista obtenida.

listam7=[i for i in range(2000,3500)if i%7==0 and i%5!=0]
print(listam7)