##Generar una lista con números al azar entre 0 y 100, donde su cantidad de elementos será un número par también obtenido al azar entre 10 y 50. Luego se solicita partir la lista por la mitad a través de la técnica de las rebanadas,
##creando dos nuevas listas.
##Imprimir las tres listas por pantalla

import random

def partirLista(lista):
    mitad1=[]
    mitad2=[]
    mitad1=lista[0:(len(lista)//2)]
    mitad2=lista[(len(lista)//2):len(lista)]
    return (mitad1,mitad2)

lista=[]
cantidad=random.randint(10,50)
while cantidad % 2 != 0:
    cantidad=random.randint(10,50)
for i in range(cantidad):
    num=random.randint(0,100)
    lista.append(num)
mitad1,mitad2=partirLista(lista)
print("La lista original es:", lista)
print("La primer mitad es :", mitad1)
print("La segunda mitad es :", mitad2)