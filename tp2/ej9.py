"""Escribir una función que reciba una lista de números enteros como parámetro y la normalice, es decir que todos sus elementos deben sumar 1.0, respetando las proporciones relativas que cada elemento tiene en la lista original.
Desarrollar también un programa que permita verificar el comportamiento de la función. Por ejemplo, normalizar([1, 1, 2]) debe devolver [0.25, 0.25, 0.50]."""

def normalizar(lista):
    sumatoria=0
    listaNorm=[]
    for i in range(len(lista)):
        sumatoria+=lista[i]
    for i in range(len(lista)):
        num=lista[i]/sumatoria
        listaNorm.append(num)
    return listaNorm

lista=[32,84,96]
listaNorm=normalizar(lista)
print(listaNorm)