#Eliminar de una lista de palabras las palabras que se encuentren en una segunda lista. Imprimir la lista original, la lista de palabras a eliminar y la lista resultante.

palabras=["Mi","vieja","mula","ya","no","es","lo","que","era"]
print("Lista de palabras original:",palabras)
eliminar=["vieja","que","es"]
print("Lista de palabras a eliminar:",eliminar)
for i in range(len(eliminar)):
    if eliminar[i] in palabras:
        pos=palabras.index(eliminar[i])
        palabras.pop(pos)
print("Lista de palabras tras eliminacion:",palabras)
