##Escribir un programa para generar una lista con los múltiplos de 7 que no sean múltiplos de 5, entre 2000 y 3500.
##Imprimir la lista obtenida.

num=2000
listam7=[]
while num<3501:
    if num % 7 == 0 and num % 5 != 0:
        listam7.append(num)
    num+=1
print("Los numeros entre 2000 y 3500 multiplos de 7 pero no de 5 son: ", listam7)