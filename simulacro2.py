#Para cualquier valor de N debe imprimirse un triángulo centrado. Ej n=5
#    *
#   * *
#  *   *
# *     *
#*********

def triangulo(n):
    ctrl_izq=n-2
    ctrl_der=1
    for j in range(n-1):
        if j==0:
            for i in range(n-1):
                print(" ",end="")
            print("*")
        else:
            for i in range(ctrl_izq):
                print(" ",end="")
            ctrl_izq-=1
            print("*",end="")
            for k in range(ctrl_der):
                print(" ",end="")
            ctrl_der+=2
            print("*")
    ctrl_der+=2    
    for m in range (ctrl_der):
        print("*",end="")
            

#Programa principal
n=int(input("Ingrese la cantidad de filas (entero impar mayor a 2): "))
while n < 3 or n%2 == 0:
    print("Incorrecto. Revise el ingreso.")
    n=int(input("Ingrese la cantidad de filas (entero impar mayor a 2): "))
triangulo(n)
