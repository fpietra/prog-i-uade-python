def convertir():
    try:
        fijo=open("fixlong.txt","rt")
        nume=open("hash.txt","rt")
        digi=open("digit.txt","rt")
        csv=open("csv.txt","wt")
        reg_fijo=fijo.readline()
        while reg_fijo:
            reg_fijo_nom=reg_fijo[:17].rstrip(" ")
            reg_fijo_leg=reg_fijo[17:26].rstrip(" ")
            reg_fijo_dir=reg_fijo[26:].rstrip(" \n")
            csv.write(reg_fijo_nom+";"+reg_fijo_leg+";"+reg_fijo_dir+"\n")
            reg_fijo=fijo.readline()
        print("El archivo fijo ha sido procesado correctamente")
        reg_hash=nume.readline().rstrip("\n")
        while reg_hash:
            hash_cad=""
            for i in range (len(reg_hash)):
                if reg_hash[i]!="#":
                    hash_cad=hash_cad+reg_hash[i]
                elif reg_hash[i]=="#":
                    hash_cad=hash_cad+";"
            csv.write(hash_cad+"\n")
            reg_hash=nume.readline().rstrip("\n")
        print("El archivo hash ha sido procesado correctamente")
        reg_digi=digi.readline()
        while reg_digi:
            reg_digi.rstrip("\n")
            ctrl_nom=int(reg_digi[:2])
            reg_digi_nom=reg_digi[2:ctrl_nom+2]
            ctrl_leg=int(reg_digi[ctrl_nom+2:ctrl_nom+4])
            reg_digi_leg=reg_digi[ctrl_nom+4:ctrl_nom+2+ctrl_leg]
            reg_digi_dir=reg_digi[ctrl_nom+4+ctrl_leg:]
            csv.write(reg_digi_nom+";"+reg_digi_leg+";"+reg_digi_dir)
            reg_digi=digi.readline()
        print("El archivo digi ha sido procesado correctamente")

    except IOError:
        print("Los archivos no han podido procesarse")

    finally:
        fijo.close()
        nume.close()
        digi.close()
        csv.close()

#Programa principal
convertir()
