##Escribir un programa que permita ingresar un conjunto de apellidos y nombres en
##formato "Apellido, Nombre" y guarde en el archivo ARMENIA.TXT los nombres de
##aquellas personas cuyo apellido termina con la cadena "IAN", en el archivo
##ITALIA.TXT los terminados en "INI" y en el archivo ESPAÑA.TXT los terminados en
##"EZ". Descartar el resto.

try:
    armenia=open("armenia.txt","wt")
    italia=open("italia.txt","wt")
    espania=open("espania.txt","wt")
    nombre_completo=input("Ingrese el nombre con el formato 'Apellido, Nombre'\n(Enter para terminar): ")
    while nombre_completo!="":
        apellido, nombre=nombre_completo.split(",")
        if apellido.find("ian",len(apellido)-3)!=-1:
            armenia.write(apellido+", "+nombre+"\n")
        elif apellido.find("ini",len(apellido)-3)!=-1:
            italia.write(apellido+", "+nombre+"\n")
        elif apellido.find("ez",len(apellido)-2)!=-1:
            espania.write(apellido+", "+nombre+"\n")
        nombre_completo=input("Ingrese el nombre con el formato 'Apellido, Nombre'\n(Enter para terminar): ")
    print("Archivos creados correctamente")

except IOError:
    print("Error de apertura de archivo")

finally:
    try:
        armenia.close()
        italia.close()
        espania.close()
    except NameError:
        pass
    except OSError:
        pass
