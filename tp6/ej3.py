def GrabarRangoAlturas():
    try:
        atletas=open("atletas.txt", "wt")
        dep=input("Ingrese el deporte (Enter para terminar): ")
        while dep!="":
            alt=input("Ingrese la altura del atleta(Enter para terminar): ")
            atletas.write(dep+"\n")
            while alt!="":
                atletas.write(alt+"\n")
                alt=input("Ingrese la altura del atleta(Enter para terminar): ")
            dep=input("Ingrese el deporte (Enter para terminar): ")
        print("Los registros se grabaron correctamente")
    except IOError:
        print("Ha fallado la grabación de datos")
    finally:
        atletas.close()

def GrabarPromedio():
    try:
        atletas=open("atletas.txt", "rt")
        promedios=open("promedios.txt", "wt")
        registro=atletas.readline()
        suma=0
        cont=0
        while registro:
            registro.rstrip("\n")
            if "A"<=registro[0]<="z":
                promedios.write(registro)
                registro=atletas.readline()
            else:
                while registro and "0"<=registro[0]<="9":
                    suma=suma+float(registro)
                    cont+=1
                    registro=atletas.readline()
                prom=suma/cont
                promedios.write(str(prom)+"\n")
                suma=0
                cont=0
            
        print("Promedios grabados correctamente")
    except IOError:
        print("Error de grabación de promedios")
    finally:
        atletas.close()
        promedios.close()

#Programa principal
#GrabarRangoAlturas()
GrabarPromedio()
