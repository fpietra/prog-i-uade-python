##Escribir un programa que permita grabar un archivo los datos de lluvia caída durante un año. Cada línea del archivo se grabará con el siguiente formato:
##<dia>;<mes>;<lluvia caída en mm> por ejemplo 25;5;319. Los datos se generarán mediante números al azar, asegurándose que las fechas
##sean válidas. La cantidad de registros también será un número al azar entre 50 y 200. Por último se solicita mostrar un informe en formato matricial donde cada columna
##represente a un mes y cada fila corresponda a los días del mes. Imprimir además el total de lluvia caída en todo el año.

import random

def generar_datos():
    try:
        arch=open("lluvias.txt", "wt")
        for i in range (random.randint(50,200)):
            m=random.randint(1,12)
            if m == 2:
                d=random.randint(1,28)
            elif m == 4 or m==6 or m== 9 or m ==11:
                d=random.randint(1,30)
            else:
                d=random.randint(1,31)
            mili=random.randint(0,999)
            reg=str(d)+";"+str(m)+";"+str(mili)+"\n"
            arch.write(reg)
        print("Registros grabados exitosamente")
    except IOError:
        print("No se puede abrir el archivo")
    finally:
        arch.close()

def generar_matriz():
    try:
        arch=open("lluvias.txt", "rt")
        registro=arch.readline()
        matriz=[]
        for f in range (31):
            matriz.append([0]*12)
        while registro:
            registro.rstrip("\n")
            f,c,mili=registro.split(";")
            registro=arch.readline()
            matriz[int(f)-1][int(c)-1]=int(mili)
        print("Los registros fueron correctamente leídos")
        return matriz
    except IOError:
        print("No se puede leer el archivo")
    finally:
        arch.close()

#Programa principal
generar_datos()
mat=generar_matriz()
total=0
print("ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SEP","OCT","NOV","DIC")
for f in range (31):
    for c in range(12):
        total=total+mat[f][c]
        print("{:3}".format(mat[f][c]),end=" ")
    print()
print()
print("En el año han llovido {} milímetros".format(total))
    

